// Bai1: Tính tiền lương nhân viên
// Đầu vào:
// -Hằng số 100.000.
// -Số ngày làm việc: 5.
// Xử lý:
// -Tạo biến cho số ngày lv, tiền lương.
// -Tiền lương = số ngày lv *100.000.
// Đầu ra:
// -Tiền lương: 500.0000.
const TIEN_LUONG = 100000;
var ngayCong = 5;
var tongLuong = ngayCong * TIEN_LUONG;
console.log("Tiền Lương: ",tongLuong);

// Bài 2:
// Đầu vào:
// - 5 số thực 1,5,7,-2,-4.
// Xử lý:
// - Tạo 5 biến và gán giá trị cho 5 số thực.
// - Tạo biến kết quả tính trung bình = (num1 + num2 + num3 +num4 +num5)/5.
// Đầu ra:
// -Trung bình cộng 5 số thực: 1.4
var num1 = 1;
var num2 = 5;
var num3 = 7;
var num4 = -2;
var num5 = -4;
var ketQua = (num1 + num2 + num3 +num4 + num5)/5;
console.log("Trung bình cộng:",ketQua);

// Bài 3:
// Đầu vào:
// -Tỷ giá: 23500;
// -Tiền đô: 5$.
// Xử lý:
// - Tạo hằng số tỷ giá. tyGia = 23500.
// - Tạo biến và gán giá trị tiền đô.
// -tạo biến tiền việt = tiền đô * tỷ giá.
// Đầu ra:
// -Giá trị tiền Việt: 117500;
const TY_GIA = 23500;
var tienDo = 5;
var tienViet = tienDo * TY_GIA;
console.log("Tiền Việt: ",tienViet);

// Bài 4:
// Đầu vào:
// -Chiều dài: 5cm.
// Chiều rộng: 3cm.
// Xử lý:
// -Tạo biến cho chiều dài và rộng.
// -Tạo biến chu vi = (Chiều dài + Chiều Rộng) *2.
// -Tạo biến diện tích = Chiều dài * Chiều rộng.
// Đầu ra:
// -Chu vi: 16cm.
// -Diện tích 15cm2
var chieuDai = 5;
var chieuRong = 3;
var chuVi = (chieuDai + chieuRong)*2;
console.log("Chu vi: ",chuVi);
var dienTich = chieuDai * chieuRong;
console.log("Dien tich: ",dienTich);


// Bài 5:
// Đầu vào:
// -Số a có 2 chữ số = 26.
// Xử lý:
// -Tạo biến cho số a.
// - Tạo biến, tách số hàng đơn vị. donVi = a%10.
// - Tạo biến, tách chữ số hàng chục:  chuc = a/10.
// -Kết quả = donVi + chuc.
// Đầu ra:
// - Kết quả = 8.
var soA = 26;
var donVi = soA % 10;
var chuc = Math.floor(soA / 10);
var ketQua = chuc + donVi;
console.log("Ket qua: ",ketQua);

